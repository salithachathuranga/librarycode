package com.nCinga;

import com.nCinga.bo.*;
import com.nCinga.bo.enums.Degree;
import com.nCinga.bo.enums.Subject;
import com.nCinga.dao.*;
import com.nCinga.service.LibraryRepositoryService;
import com.nCinga.service.impl.LibraryRepositoryServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Admin admin1 = new Admin(123, "Maths Library Admin");
        Admin admin2 = new Admin(444, "Arts Library Admin");
        Admin admin3 = new Admin(456, "Science Library Admin");
        AdminDao.getInstance().addAdmin(admin1);
        AdminDao.getInstance().addAdmin(admin2);
        AdminDao.getInstance().addAdmin(admin3);

        Library library1 = new Library(234, "Science Library");
        Library library2 = new Library(678, "Arts Library");
        LibraryDao.getInstance().addLibrary(library1);
        LibraryDao.getInstance().addLibrary(library2);

        Student student1 = new Student(123, "salitha", "2015", "UCSC", Degree.B_TECH);
        Student student2 = new Student(455, "charith", "2015", "ENTC", Degree.B_TECH);
        Student student3 = new Student(237, "yasas", "2015", "UCSC", Degree.M_TECH);
        Student student4 = new Student(566, "heshan", "2015", "ENTC", Degree.M_TECH);
        StudentDao.getInstance().addStudent(student1);
        StudentDao.getInstance().addStudent(student2);
        StudentDao.getInstance().addStudent(student3);
        StudentDao.getInstance().addStudent(student4);

        Book book1 = new Book("Biology Fundamentals", Subject.BIO);
        Book book2 = new Book("Heat Basics", Subject.PHYSICS);
        Book book3 = new Book("Trigonometry", Subject.MATHS);
        LibraryBookRecord bookRecord1 = new LibraryBookRecord(book1,library1,10,3);
        LibraryBookRecord bookRecord2 = new LibraryBookRecord(book2,library1,5,2);
        LibraryBookRecord bookRecord3 = new LibraryBookRecord(book3,library1,20,15);
        LibraryBookRecordDao.getInstance().addLibraryBookRecord(bookRecord1);
        LibraryBookRecordDao.getInstance().addLibraryBookRecord(bookRecord2);
        LibraryBookRecordDao.getInstance().addLibraryBookRecord(bookRecord3);

        LibraryRepositoryService service = new LibraryRepositoryServiceImpl();

//        service.issueBookAfterValidation(book1,student1,admin1,library1);
//        System.out.println(IssuedBookRecordDao.getInstance().getIssuedBookRecords());
//        System.out.println(LibraryBookRecordDao.getInstance().getBookRecords());
//        System.out.println(student1);
//
//        System.out.println("===============================");
//
//        service.issueBookAfterValidation(book1,student1,admin1,library1);
//        System.out.println(IssuedBookRecordDao.getInstance().getIssuedBookRecords());
//        System.out.println(LibraryBookRecordDao.getInstance().getBookRecords());
//        System.out.println(student1);
//
//        System.out.println("===============================");

//        System.out.println(LibraryBookRecordDao.getInstance().getBookRecords());
        for (LibraryBookRecord lib:
                LibraryBookRecordDao.getInstance().getBookRecords()) {
            System.out.println(lib);
        }

        service.issueBookAfterValidation(book2,student1,admin1,library1);
//        System.out.println(IssuedBookRecordDao.getInstance().getIssuedBookRecords());
//        System.out.println(LibraryBookRecordDao.getInstance().getBookRecords());
//        System.out.println(student1);


//        service.returnBookAfterValidation(book1,student1,admin1,library1);
//        System.out.println(student1);
//        System.out.println(IssuedBookRecordDao.getInstance().getIssuedBookRecords());
//        System.out.println(LibraryBookRecordDao.getInstance().getBookRecords());

    }
}
