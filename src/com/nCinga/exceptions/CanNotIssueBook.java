package com.nCinga.exceptions;

public class CanNotIssueBook extends RuntimeException {

    public CanNotIssueBook(String msg) {
        super(msg);
    }
}
