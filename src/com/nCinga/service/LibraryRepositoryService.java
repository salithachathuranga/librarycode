package com.nCinga.service;

import com.nCinga.bo.*;

public interface LibraryRepositoryService {

    void issueBookAfterValidation(Book book, Student student, Admin admin, Library library);
    void returnBookAfterValidation(Book book, Student student, Admin admin, Library library);
}
