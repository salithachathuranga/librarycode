package com.nCinga.service.impl;

import com.nCinga.bo.*;
import com.nCinga.dao.*;
import com.nCinga.exceptions.CanNotIssueBook;
import com.nCinga.exceptions.NotFoundException;
import com.nCinga.service.LibraryRepositoryService;

public class LibraryRepositoryServiceImpl implements LibraryRepositoryService {

    AdminDao adminDao;
    LibraryDao libraryDao;
    StudentDao studentDao;
    LibraryBookRecordDao libraryBookRecordDao;
    IssuedBookRecordDao issuedBookRecordDao;

    public LibraryRepositoryServiceImpl() {
        adminDao = AdminDao.getInstance();
        libraryDao = LibraryDao.getInstance();
        studentDao = StudentDao.getInstance();
        libraryBookRecordDao = LibraryBookRecordDao.getInstance();
        issuedBookRecordDao = IssuedBookRecordDao.getInstance();
    }

    public void issueBookFromLibraryToStudent(Book book, Student student, Admin admin, Library library){
        issuedBookRecordDao.addIssuedBookRecord(new IssuedBookRecord(student,book,admin));
        System.out.println("book: "+ book);
        libraryBookRecordDao.findLibraryBookRecord(library,book).increaseIssuedCountOfLibraryBook(book);
        studentDao.findStudentById(student.getRollNo()).increaseBorrowedBookCountByOne();
    }

    public boolean validateBookToBeIssuedForStudent(Book book, Student student, Admin admin, Library library){
        return (
                libraryBookRecordDao.isLibraryBookAvailableToBeBorrowed(library,book) &&
                libraryBookRecordDao.isLibraryBookExists(library,book) &&
                libraryDao.isLibraryExists(library) &&
                adminDao.isAdminExists(admin) &&
                studentDao.isStudentExists(student) &&
                issuedBookRecordDao.isStudentEligibleForIssue(student)
        );
    }

    public void issueBookAfterValidation(Book book, Student student, Admin admin, Library library) {
        if(validateBookToBeIssuedForStudent(book,student,admin,library)){
            issueBookFromLibraryToStudent(book,student,admin,library);
        }
        else {
            throw new CanNotIssueBook("Book can not be issued to the student");
        }
    }

    public void returnBookAfterValidation(Book book, Student student, Admin admin, Library library) {
        IssuedBookRecord record;
        if(libraryBookRecordDao.isLibraryBookExists(library,book)){
            if(issuedBookRecordDao.isIssuedBookRecordExists(book,student,admin)){
                record = issuedBookRecordDao.findIssuedBookRecordRecord(book,student,admin);
                issuedBookRecordDao.removeIssuedBookRecord(record);
                libraryBookRecordDao.findLibraryBookRecord(library,book).decreaseIssuedCountOfLibraryBook();
                studentDao.findStudentById(student.getRollNo()).decreaseBorrowedBookCountByOne();
            }
            else {
                throw new NotFoundException("issuedBookRecord can not be found");
            }
        }
        else {
            throw new NotFoundException("LibraryBookRecord can not be found");
        }

    }
}
