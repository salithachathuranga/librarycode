package com.nCinga.dao;

import com.nCinga.bo.Student;
import com.nCinga.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {

    List<Student> students;
    private static StudentDao studentDao;

    public StudentDao() {
        this.students = new ArrayList<>();
    }

    public static StudentDao getInstance(){
        studentDao = studentDao == null ? new StudentDao() : studentDao;
        return studentDao;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student){
        this.students.add(student);
    }

    public void removeStudent(Student student){
        this.students.remove(student);
    }

    public Student findStudentById(int id){
        for (Student student : students) {
            if(student.getRollNo() == id){
                return student;
            }
        }
        throw new NotFoundException("Student can not be found by the id: "+id);
    }

    public Student findStudentByName(String name){
        for (Student student : students) {
            if(student.getName().equals(name)){
                return student;
            }
        }
        throw new NotFoundException("Student can not be found by the name: "+name);
    }

    public boolean isStudentExists(Student student){
        if(students.contains(student)){
            return true;
        }
        else {
            throw new NotFoundException("Student can not be found");
        }
    }

    public void updateStudent(Student existingStudent, Student newStudent) {
        int indexOfStudent = students.indexOf(existingStudent);
        if (indexOfStudent != -1){
            students.set(indexOfStudent, newStudent);
        }
    }

}
