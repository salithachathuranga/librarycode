package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.bo.Library;
import com.nCinga.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class LibraryDao {

    private List<Library> libraries;
    private static LibraryDao libraryDao;

    public LibraryDao() {
        this.libraries = new ArrayList<>();
    }

    public static LibraryDao getInstance(){
        libraryDao = libraryDao == null ? new LibraryDao() : libraryDao;
        return libraryDao;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public void addLibrary(Library library){
        this.libraries.add(library);
    }

    public void removeLibrary(Library library){
        this.libraries.remove(library);
    }

    public Library findLibraryById(int id){
        for (Library library : libraries) {
            if(library.getLibraryId() == id){
                return library;
            }
        }
        throw new NotFoundException("Library can not be found by the ID: "+id);
    }

    public Library findLibraryByName(String name){
        for (Library library : libraries) {
            if(library.getLibraryName().equals(name)){
                return library;
            }
        }
        throw new NotFoundException("Library can not be found by the Name: "+name);
    }

    public boolean isLibraryExists(Library library){
        if(libraries.contains(library)){
            return true;
        }
        else {
            throw new NotFoundException("Library can not be found");
        }
    }

    public void updateLibrary(Library existingLibrary, Library newLibrary) {
        int indexOfLibrary = libraries.indexOf(existingLibrary);
        if (indexOfLibrary != -1){
            libraries.set(indexOfLibrary, newLibrary);
        }
    }

}
