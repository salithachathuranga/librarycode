package com.nCinga.dao;

import com.nCinga.bo.Book;
import com.nCinga.bo.Library;
import com.nCinga.bo.LibraryBookRecord;
import com.nCinga.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class LibraryBookRecordDao {

    private List<LibraryBookRecord> bookRecords;
    private static LibraryBookRecordDao libraryBookRecordDao;

    public LibraryBookRecordDao() {
         this.bookRecords = new ArrayList<>();
    }

    public static LibraryBookRecordDao getInstance(){
        libraryBookRecordDao = libraryBookRecordDao == null ? new LibraryBookRecordDao() : libraryBookRecordDao;
        return libraryBookRecordDao;
    }

    public List<LibraryBookRecord> getBookRecords() {
        return bookRecords;
    }

    public void addLibraryBookRecord(LibraryBookRecord record){
        this.bookRecords.add(record);
    }

    public void removeLibraryBookRecord(LibraryBookRecord record){
        this.bookRecords.remove(record);
    }

    public LibraryBookRecord findLibraryBookRecord(Library library, Book book){
        LibraryBookRecord libraryBookRecord;
        for (LibraryBookRecord record : bookRecords) {
            if(record.getBook().equals(book) && record.getLibrary().equals(library)){
                libraryBookRecord = record;
                return libraryBookRecord;
            }
            else {
                throw new NotFoundException("LibraryBookRecord can not be found");
            }
        }
        return null;
    }

    public boolean isLibraryBookExists(Library library, Book book){
        if(bookRecords.size() > 0) {
            LibraryBookRecord libraryBookRecord = findLibraryBookRecord(library,book);
            if (libraryBookRecord != null){
                return true;
            }
        }
        throw new NotFoundException("LibraryBookRecord can not be found");
    }

    public boolean isLibraryBookAvailableToBeBorrowed(Library library, Book book){
        if((isLibraryBookExists(library,book)) && (bookRecords.size() > 0)) {
            LibraryBookRecord libraryBookRecord = findLibraryBookRecord(library, book);
            int availableCopiesCount = libraryBookRecord.getTotalBookCount() - libraryBookRecord.getIssuedBookCount();
            if (availableCopiesCount != 0){
                return true;
            }
        }
        return false;
    }

//    public void increaseIssuedCountOfLibraryBook(Book book, Library library){
//        LibraryBookRecord record = findLibraryBookRecord(library,book);
//        System.out.println(record);
//        if(isLibraryBookExists(library,book)){
//            System.out.println(record.getIssuedBookCount());
//        }
//    }

}
