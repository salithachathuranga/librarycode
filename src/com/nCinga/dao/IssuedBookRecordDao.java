package com.nCinga.dao;

import com.nCinga.bo.*;
import com.nCinga.exceptions.CanNotIssueBook;
import com.nCinga.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class IssuedBookRecordDao {

    private List<IssuedBookRecord> issuedBookRecords;
    private static IssuedBookRecordDao issuedBookRecordDao;

    public IssuedBookRecordDao() {
        this.issuedBookRecords = new ArrayList<>();
    }

    public static IssuedBookRecordDao getInstance(){
        issuedBookRecordDao = issuedBookRecordDao == null ? new IssuedBookRecordDao() : issuedBookRecordDao;
        return issuedBookRecordDao;
    }

    public void addIssuedBookRecord(IssuedBookRecord record){
        System.out.println(record);
        this.issuedBookRecords.add(record);
    }

    public void removeIssuedBookRecord(IssuedBookRecord record){
        this.issuedBookRecords.remove(record);
    }

    public IssuedBookRecord findIssuedBookRecordById(int id){
        for (IssuedBookRecord issuedBookRecord : issuedBookRecords) {
            if(issuedBookRecord.getIssueId() == id){
                return issuedBookRecord;
            }
        }
        throw new NotFoundException("IssuedBookRecord can not be found by the id: "+id);
    }

    private int getIssuedBookCountOfStudent(Student student){
        int totalIssuedCount = 0;
        for (IssuedBookRecord record:issuedBookRecords) {
            if(record.getStudent().getRollNo() == student.getRollNo()){
                ++totalIssuedCount;
            }
        }
        return totalIssuedCount;
    }

    public IssuedBookRecord findIssuedBookRecordRecord(Book book, Student student, Admin admin){
        IssuedBookRecord issuedBookRecord;
        for (IssuedBookRecord record : issuedBookRecords) {
            if(record.getBook().equals(book) && record.getStudent().equals(student) && record.getAdmin().equals(admin)){
                issuedBookRecord = record;
                return issuedBookRecord;
            }
            else {
                throw new NotFoundException("IssuedBookRecord can not be found in the records");
            }
        }
        return null;
    }

    public boolean isIssuedBookRecordExists(Book book, Student student, Admin admin){
        IssuedBookRecord record = findIssuedBookRecordRecord(book, student, admin);
        if(issuedBookRecords.contains(record)){
            return true;
        }
        throw new NotFoundException("IssuedBookRecord can not be found in the records");
    }

    public List<IssuedBookRecord> getIssuedBookRecords() {
        return issuedBookRecords;
    }

    public boolean isStudentEligibleForIssue(Student student){
        if (getIssuedBookCountOfStudent(student) > student.getMaximumIssuedBookCountForDegree()){
            throw new CanNotIssueBook("Student has exceeded the limit of books to be borrowed");
        }
        else {
            return true;
        }
    }
}
