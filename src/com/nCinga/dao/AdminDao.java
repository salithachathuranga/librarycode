package com.nCinga.dao;

import com.nCinga.bo.Admin;
import com.nCinga.exceptions.NotFoundException;

import java.util.ArrayList;
import java.util.List;

public class AdminDao {

    private static AdminDao adminDao;
    private List<Admin> admins;

    private AdminDao() {
        admins = new ArrayList<>();admins.add(new Admin(2, "xxx"));
    }

    public static AdminDao getInstance(){
        adminDao = adminDao == null ? new AdminDao() : adminDao;
        return adminDao;
    }

    public List<Admin> getAdmins() {
        return admins;
    }

    public void addAdmin(Admin admin){
        admins.add(admin);
    }

    public void removeAdmin(Admin admin){
        admins.remove(admin);
    }

    public Admin findAdminById(int id){
        for (Admin admin : admins) {
            if(admin.getId() == id){
                return admin;
            }
        }
        throw new NotFoundException("Admin can not be found by the ID: "+id);
    }
    public Admin findAdminByName(String name){
        for (Admin admin : admins) {
            if(admin.getName().equals(name)){
                return admin;
            }
        }
        throw new NotFoundException("Admin can not be found by the name: "+name);
    }

    public boolean isAdminExists(Admin admin){
        if(admins.contains(admin)){
            return true;
        }
        else {
            throw new NotFoundException("Admin can not be found");
        }
    }

    public void updateAdmin(Admin existingAdmin, Admin newAdmin) {
        int indexOfAdmin = admins.indexOf(existingAdmin);
        if (indexOfAdmin != -1){
            admins.set(indexOfAdmin, newAdmin);
        }
    }

}
