package com.nCinga.bo;

import java.util.Objects;

public class Library {

    private final int libraryId;
    private final String libraryName;

    public Library(int libraryId, String libraryName) {
        this.libraryId = libraryId;
        this.libraryName = libraryName;
    }

    public int getLibraryId() {
        return libraryId;
    }

    public String getLibraryName() {
        return libraryName;
    }

    public String toString() {
        return "Library{libraryId=" + libraryId + ", libraryName=" + libraryName + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Library library = (Library) o;
        return libraryId == library.libraryId && libraryName.equals(library.libraryName);
    }

    public int hashCode() {
        return Objects.hash(libraryId, libraryName);
    }
}
