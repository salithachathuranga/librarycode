package com.nCinga.bo;

import java.util.Objects;

public class IssuedBookRecord {

    private int issueId = 0;
    private static int autoIncId = 0;
    private final Student student;
    private final Book book;
    private final Admin admin;

    public IssuedBookRecord(Student student, Book book, Admin admin) {
        this.student = student;
        this.book = book;
        this.admin = admin;
        this.issueId = autoIncrementId();
    }

    public int getIssueId() {
        return issueId;
    }

    public int autoIncrementId() {
        return ++autoIncId;
    }

    public Student getStudent() {
        return student;
    }

    public Book getBook() {
        return book;
    }

    public Admin getAdmin() {
        return admin;
    }

    public String toString() {
        return "IssuedBookRecord{" +
                "issueId=" + issueId +
                ", student=" + student +
                ", book=" + book +
                ", admin=" + admin +
                '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IssuedBookRecord that = (IssuedBookRecord) o;
        return issueId == that.issueId &&
                student.equals(that.student) &&
                book.equals(that.book) &&
                admin.equals(that.admin);
    }

    public int hashCode() {
        return Objects.hash(issueId, student, book, admin);
    }
}
