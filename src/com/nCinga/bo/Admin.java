package com.nCinga.bo;

import java.util.Objects;

public class Admin {

    private final String name;
    private final int id;

    public Admin(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return "Admin{name=" + name + ", id=" + id + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Admin admin = (Admin) o;
        return id == admin.id && name.equals(admin.name);
    }

    public int hashCode() {
        return Objects.hash(name, id);
    }
}
