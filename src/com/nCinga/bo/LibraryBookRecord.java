package com.nCinga.bo;

import java.util.Objects;

public class LibraryBookRecord {

    private final Book book;
    private final Library library;
    private final int totalBookCount;
    private int issuedBookCount;


    public LibraryBookRecord(Book book, Library library, int totalBookCount, int issuedBookCount) {
        this.book = book;
        this.library = library;
        this.totalBookCount = totalBookCount;
        this.issuedBookCount = issuedBookCount;
    }

    public void increaseIssuedCountOfLibraryBook(Book book){
        issuedBookCount++;
    }

    public void decreaseIssuedCountOfLibraryBook(){
        issuedBookCount--;
    }

    private void setIssuedBookCount(){
    issuedBookCount++;
    }

    public Book getBook() {
        return book;
    }

    public Library getLibrary() {
        return library;
    }

    public int getTotalBookCount() {
        return totalBookCount;
    }

    public int getIssuedBookCount() {
        return issuedBookCount;
    }

    public String toString() {
        return "LibraryBookRecord{book=" + book +
                ", library=" + library +
                ", totalBookCount=" + totalBookCount +
                ", issuedBookCount=" + issuedBookCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryBookRecord that = (LibraryBookRecord) o;
        return totalBookCount == that.totalBookCount &&
                book.equals(that.book) &&
                library.equals(that.library);
    }

    @Override
    public int hashCode() {
        return Objects.hash(book, library, totalBookCount, issuedBookCount);
    }
}
