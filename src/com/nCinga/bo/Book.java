package com.nCinga.bo;

import com.nCinga.bo.enums.Subject;

import java.util.Objects;

public class Book {

    private final String name;
    private final Subject subject;

    public Book(String name, Subject subject) {
        this.name = name;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public Subject getSubject() {
        return subject;
    }


    public String toString() {
        return "Book{name=" + name + ", subject=" + subject + "}";
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return name.equals(book.name) && subject == book.subject;
    }

    public int hashCode() {
        return Objects.hash(name, subject);
    }
}
