package com.nCinga.bo;

import com.nCinga.bo.enums.Degree;

import java.util.Objects;

public class Student {

    private final int rollNo;
    private final String name;
    private final String batch;
    private final String section;
    private final Degree degree;
    private int borrowedBookCount;

    public static final int MAX_B_TECH_BOOK_COUNT = 5;
    public static final int MAX_M_TECH_BOOK_COUNT = 10;

    public Student(int rollNo, String name, String batch, String section, Degree degree) {
        this.rollNo = rollNo;
        this.name = name;
        this.batch = batch;
        this.section = section;
        this.degree = degree;
    }

    public int getRollNo() {
        return rollNo;
    }

    public String getName() {
        return name;
    }

    public String getBatch() {
        return batch;
    }

    public String getSection() {
        return section;
    }

    public Degree getDegree() {
        return degree;
    }

    public int getBorrowedBookCount() {
        return borrowedBookCount;
    }

    public void increaseBorrowedBookCountByOne(){
        borrowedBookCount++;
    }

    public void decreaseBorrowedBookCountByOne(){
        borrowedBookCount--;
    }

    public String toString() {
        return "Student{rollNo=" + rollNo + ", name=" + name + ", batch=" + batch + ", section=" + section + ", degree=" + degree + ", borrowedBookCount=" + borrowedBookCount + "}";
    }

    public int getMaximumIssuedBookCountForDegree() {
        if (this.getDegree() == Degree.B_TECH)
            return MAX_B_TECH_BOOK_COUNT;
        else if (this.getDegree() == Degree.M_TECH)
            return MAX_M_TECH_BOOK_COUNT;
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return rollNo == student.rollNo &&
                name.equals(student.name) &&
                batch.equals(student.batch) &&
                section.equals(student.section) &&
                degree == student.degree &&
                borrowedBookCount == student.borrowedBookCount;
    }

    public int hashCode() {
        return Objects.hash(rollNo, name);
    }
}
